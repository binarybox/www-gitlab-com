---
layout: markdown_page
title: "Configuration Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Configuration Team

The configuration team is responsible for the Kubernetes features in the GitLab platform.
