---
layout: markdown_page
title: "Engineering Management"
---

## How Engineering Management Works at GitLab

At GitLab, we promote two paths for leadership in Engineering. While there is a
healthy degree of overlap between these two ideas, it is helpful and efficient
for us to specialize training and responsibility for each of:

- **Technical leadership**, as represented by [Staff and higher-level
  developers](/job-families/engineering/developer/#staff-developer).
- **Professional leadership**, as represented by [Engineering
  management](/job-families/engineering/engineering-management/).

While technical leadership tends to come naturally to software engineers,
professional leadership can be more difficult to master. This page will serve as
a training resource and operational guide for current and future managers.

## On this page
{:.no_toc}

- TOC
{:toc}

## General leadership principles

All Engineering Managers should follow the [general leadership
principles](/handbook/leadership) set out in the handbook. In particular, it is
not uncommon for Engineering Managers to struggle with one or more of the
following areas, so we recommend you review them carefully and discuss your
confidence with your manager:

- [1-1s](/handbook/leadership/1-1)
- [Providing regular feedback](handbook/leadership/#giving-performance-feedback)
- [Dealing with underperformance](/handbook/underperformance/)

## Hiring

- **Hiring is a priority.** GitLab is a fast-growing company, and paying
  attention to hiring is one of the highest-leverage activities a manager can
  perform. As long as there are vacancies on your team, hiring should be your top
  priority.
- **Hiring is your responsibility.** You may rely on other members of your team
  to help you evaluate potential candidates. We also have many people at GitLab,
  most notably in Recruiting, who will help you through the process. But it is
  always your responsibility to make timely, high-quality hires - speak up if
  you feel like anything is preventing you from accomplishing this goal.
- **Hiring is about making the team better.** It is easy to try to find
  candidates who can merely perform the functions of the role, and more
  difficult to find people who make the team better while they do it.
- **Hiring is hard.** It often involves making difficult decisions, and learning
  to do it well can take years. In times of rapid growth, it may consume 50% or
  more of your time. Please use every resource available to you, from
  more experienced hiring managers in the company to the many resources
  [available in our handbook](/handbook/hiring/interviewing/).

## Team retrospectives

In addition to the [public function-wide
retrospective](handbook/engineering/workflow/#retrospective), each Engineering
team should be in the practice of holding their own retrospectives. The results
of these retrospectives should then inform the function-wide retrospective for
any given release. [More information on running effective retrospectives is
available here](/handbook/engineering/management/team-retrospectives).
