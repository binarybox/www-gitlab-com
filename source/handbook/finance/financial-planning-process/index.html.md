---
layout: markdown_page
title: "Financial Planning Process"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Financial Planning Process

At GitLab, we run a rolling 4 quarter forecast process. This means that we are always looking out a minimum of 12 months when projecting revenue and expenses, and we update those forecasts at least once per quarter. Things move quickly and our forecast needs to iterate quickly to keep up with the business. We plan our expenses at a high level (e-group) and we expect this group to make prioritizations and trade-offs while remaining accountable against the budget parameters. By reforecasting quarterly, we can quickly evaluate and incorporate new initiatives into our forecasting model. That being said, we do follow an annual plan to set our goals and measurement for our top-level targets of revenue, profitability and expense management. We follow the cadence below in our planning process:

****

### Monthly Update
* Hold Departmental Metrics Meetings to review how well the dedepartments are operating. 
* Send out Investors Update
* Update our Budget vs Actual model with revenue, expense and headcount actuals.
* Perform an actuals vs  budget/forecast variance analysis.
* Distribute monthly results to budget owners.

###### Important Dates
* **10th of the Month** - Update Metrics Report
* **10th of the Month** - Update Revenue Model
* **10th of the Month** - Send out Investors Update
* **15th of the Month** - Update Financial Models
* **15th of the Month** - Distribute monthly results to budget owners

****

### Quarterly Forecast
* All of the activities in the Monthly Forecast.
* Budget owners update headcount planning templates and non-headcount expenses.
* Revenue model updated and signed off by CMO and CRO.

###### Important Dates
* **14th of the Last Month in Fiscal Quarter** - Send out calender invites to review non-headcount & headcount expenses 
* **22th of the Last Month in Fiscal Quarter** - Finalize non-headcount & headcount planning with departments
* **22th of the Month** - Finalize Revenue Model signoff from CMO and CRO

****

### Annual Plan
* All of activities above.
* Revise and update the annual sales compensation plan.
* Set annual quota assignments for revenue producing roles.
* Review product investments vs expected revenue generation.
* Set expected amount for annual compensation increases.
* Set targets for any contributors on a company based performance plan.
* Set company targets for board, investors and creditors.
* Our Annual Plan is viewable internally as a google slide presentation.  Search on "[current year e.g. 2018] Plan" to view.
