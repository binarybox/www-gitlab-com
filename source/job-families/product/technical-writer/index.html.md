---
layout: job_page
title: "Technical Writer"
---

At GitLab, our team of technical writers is responsible for ensuring that the [documentation](https://docs.gitlab.com/) for all of our products is clear, correct, and complete. We are looking for great writers with strong technical proficiencies who will empower our users to succeed with our evolving suite of developer tools.

You’ll collaborate with our engineers, who will typically write the first draft of docs for the new features they create. You’ll dive in on special projects, authoring new content and improving existing resources. You’ll collaborate with others across the organization to craft tutorials and other engaging educational resources. You’ll be at the leading edge of DevOps while contributing to one of the [world’s largest open-source ](/2017/07/06/gitlab-top-30-highest-velocity-open-source/)projects.

This is a remote position with no unique geographical requirements.

## Responsibilities

### Documentation

* Collaborate with engineers to produce and improve our documentation content.
* Ensure new features are documented while addressing content gaps and other issues reported by our team and community.
* Improve the documentation user experience.

### Tutorials

* Produce written and video tutorials for getting started with GitLab features and on advanced topics.

### Website

* Keep technical content on GitLab’s [website](/) up to date.
* Review [Community Writers Program](/community-writers/) pitches and merge requests.

## Requirements

* Affinity for managing and writing software documentation
* Understanding of what makes documentation clear and effective
* Excellent writing skills
* Great teaching skills that translate into amazing written work
* Highly organized
* Experience using git
* Familiarity with the command line in Linux or Mac
* You share our [values](/handbook/values), and work in accordance with those values

## Bonus Points

* Experience with static-site generators
* Experience with one or more DevOps tools